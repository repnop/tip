use crate::{
    parser::ParseError,
    token::{Token, TokenKind},
};
use codespan::ByteSpan;
use lazy_static::lazy_static;
use std::{convert::TryFrom, fmt, sync::Mutex};
use string_interner::{DefaultStringInterner, Sym};

lazy_static! {
    pub static ref GLOBAL_INTERNER: Mutex<DefaultStringInterner> =
        Mutex::new(DefaultStringInterner::default());
}

#[derive(Clone, Copy, Eq)]
pub struct Identifier {
    sym: Sym,
    pub span: ByteSpan,
}

impl Identifier {
    pub fn new(ident: &str, span: ByteSpan) -> Self {
        let sym = GLOBAL_INTERNER.lock().unwrap().get_or_intern(ident);

        Self { sym, span }
    }
}

impl std::cmp::PartialEq for Identifier {
    fn eq(&self, other: &Identifier) -> bool {
        self.sym == other.sym
    }
}

impl std::hash::Hash for Identifier {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.sym.hash(state);
    }
}

impl fmt::Display for Identifier {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            GLOBAL_INTERNER.lock().unwrap().resolve(self.sym).unwrap()
        )
    }
}

impl fmt::Debug for Identifier {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{:?}",
            GLOBAL_INTERNER.lock().unwrap().resolve(self.sym).unwrap()
        )
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Integer {
    pub value: isize,
    pub span: ByteSpan,
}

impl std::ops::Neg for Integer {
    type Output = Integer;

    fn neg(self) -> Self {
        Integer {
            value: -self.value,
            span: self.span,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Expression {
    pub kind: ExpressionKind,
    pub span: ByteSpan,
}

impl Expression {
    pub fn new(kind: ExpressionKind, span: ByteSpan) -> Self {
        Self { kind, span }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum ExpressionKind {
    Integer(Integer),
    Identifier(Identifier),
    Binary(Box<Expression>, BinaryOp, Box<Expression>),
    Unary(UnaryOp, Box<Expression>),
    Record(Vec<(Identifier, Expression)>),
    Field(Box<Expression>, Identifier),
    Call(Box<Expression>, Vec<Expression>),
    Input,
    Null,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum BinaryOp {
    Paren,
    Dot,
    Add,
    Sub,
    Mult,
    Div,
    Gt,
    Eq,
}

impl BinaryOp {
    pub fn precedence(self) -> u8 {
        use self::BinaryOp::*;
        match self {
            Dot => 4,
            Paren => 3,
            Mult => 2,
            Div => 2,
            Add => 1,
            Sub => 1,
            Eq => 0,
            Gt => 0,
        }
    }
}

impl<'a> TryFrom<Token<'a>> for BinaryOp {
    type Error = ParseError<'a>;

    fn try_from(kind: Token<'a>) -> Result<BinaryOp, ParseError<'a>> {
        Ok(match kind.kind {
            TokenKind::LParen => BinaryOp::Paren,
            TokenKind::Dot => BinaryOp::Dot,
            TokenKind::Plus => BinaryOp::Add,
            TokenKind::Minus => BinaryOp::Sub,
            TokenKind::Star => BinaryOp::Mult,
            TokenKind::Div => BinaryOp::Div,
            TokenKind::Equal => BinaryOp::Eq,
            TokenKind::GreaterThan => BinaryOp::Gt,
            _ => return Err(kind.into()),
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum UnaryOp {
    Alloc,
    Ref,
    Deref,
    Negate,
}

pub type Statements = Vec<Statement>;

#[derive(Clone, Debug, PartialEq)]
pub struct Statement {
    pub kind: StatementKind,
    pub span: ByteSpan,
}

#[derive(Clone, Debug, PartialEq)]
pub enum StatementKind {
    Assignment(Identifier, Expression),
    AssignThrough(Identifier, Expression),
    Output(Expression),
    While(Expression, Statements),
    If(Expression, Statements, Option<Statements>),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Function {
    pub name: Identifier,
    pub parameters: Vec<Identifier>,
    pub locals: Vec<Identifier>,
    pub body: Statements,
    pub ret: Expression,
    pub span: ByteSpan,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Program(pub Vec<Function>);
