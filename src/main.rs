#![allow(dead_code)]

mod ast;
mod parser;
mod token;
mod visit;

use std::str::FromStr;
use visit::{UndeclaredIdentifierVisitor, Visitor};

fn main() {
    let filename = &std::env::args().nth(1).unwrap();
    let mut codemap = codespan::CodeMap::<String>::new();
    let filemap = codemap.add_filemap_from_disk(filename).unwrap();

    match parser::Parser::new(codemap.iter().nth(0).unwrap().src()).parse_program() {
        Ok(mut program) => {
            match UndeclaredIdentifierVisitor::default().visit_program(&mut program) {
                Ok(_) => {}
                Err(e) => {
                    let diagnostic = codespan_reporting::Diagnostic::new(
                        codespan_reporting::Severity::Error,
                        e.0.to_string(),
                    )
                    .with_label(codespan_reporting::Label::new(
                        e.1,
                        codespan_reporting::LabelStyle::Primary,
                    ));
                    let mut ss = codespan_reporting::termcolor::StandardStream::stderr(
                        codespan_reporting::termcolor::ColorChoice::Auto,
                    );

                    codespan_reporting::emit(&mut ss, &codemap, &diagnostic).unwrap();
                }
            }
        }
        Err(e) => {
            let diagnostic = codespan_reporting::Diagnostic::new(
                codespan_reporting::Severity::Error,
                e.msg.to_string(),
            )
            .with_label(codespan_reporting::Label::new(
                e.span(),
                codespan_reporting::LabelStyle::Primary,
            ));
            let mut ss = codespan_reporting::termcolor::StandardStream::stderr(
                codespan_reporting::termcolor::ColorChoice::Auto,
            );

            codespan_reporting::emit(&mut ss, &codemap, &diagnostic).unwrap();
        }
    };
}
