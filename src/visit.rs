use crate::ast::*;
use std::{collections::HashSet, error::Error};

pub type VResult = Result<(), (Box<dyn Error>, codespan::ByteSpan)>;

pub trait Visitor {
    fn visit_identifier(&mut self, ident: &mut Identifier) -> VResult;
    fn visit_integer(&mut self, int: &mut Integer) -> VResult;
    fn visit_expression(&mut self, expression: &mut Expression) -> VResult;
    fn visit_statement(&mut self, statement: &mut Statement) -> VResult;
    fn visit_function(&mut self, function: &mut Function) -> VResult;
    fn visit_program(&mut self, program: &mut Program) -> VResult;
}

#[derive(Default)]
pub struct UndeclaredIdentifierVisitor {
    functions: HashSet<Identifier>,
    locals: HashSet<Identifier>,
}

impl UndeclaredIdentifierVisitor {
    fn ident_exists(&self, ident: Identifier) -> VResult {
        if self.locals.get(&ident).is_none() && self.functions.get(&ident).is_none() {
            return Err((format!("Unknown identifier '{}'", ident).into(), ident.span));
        }

        Ok(())
    }
}

impl Visitor for UndeclaredIdentifierVisitor {
    fn visit_program(&mut self, program: &mut Program) -> VResult {
        for function in &mut program.0 {
            self.functions.insert(function.name);
        }

        for function in &mut program.0 {
            self.visit_function(function)?;
        }

        Ok(())
    }

    fn visit_function(&mut self, function: &mut Function) -> VResult {
        self.locals.clear();

        function.locals.iter().cloned().for_each(|ident| {
            self.locals.insert(ident);
        });
        function.parameters.iter().cloned().for_each(|ident| {
            self.locals.insert(ident);
        });

        for statement in &mut function.body {
            self.visit_statement(statement)?;
        }

        self.visit_expression(&mut function.ret)?;

        Ok(())
    }

    fn visit_statement(&mut self, statement: &mut Statement) -> VResult {
        match &mut statement.kind {
            StatementKind::Assignment(ident, expr) => {
                self.ident_exists(*ident)?;
                self.visit_expression(expr)
            }
            StatementKind::AssignThrough(ident, expr) => {
                self.ident_exists(*ident)?;
                self.visit_expression(expr)
            }
            StatementKind::Output(expr) => self.visit_expression(expr),
            StatementKind::If(expr, stmts, otherwise) => {
                self.visit_expression(expr)?;
                stmts
                    .iter_mut()
                    .try_for_each(|stmt| self.visit_statement(stmt))?;

                if let Some(stmts) = otherwise {
                    stmts
                        .iter_mut()
                        .try_for_each(|stmt| self.visit_statement(stmt))?;
                }

                Ok(())
            }
            StatementKind::While(expr, stmts) => {
                self.visit_expression(expr)?;
                stmts
                    .iter_mut()
                    .try_for_each(|stmt| self.visit_statement(stmt))
            }
        }
    }

    fn visit_expression(&mut self, expr: &mut Expression) -> VResult {
        match &mut expr.kind {
            ExpressionKind::Binary(e1, _, e2) => {
                self.visit_expression(e1)?;
                self.visit_expression(e2)
            }
            ExpressionKind::Call(e1, exprs) => {
                if let ExpressionKind::Identifier(ident) = &mut (**e1).kind {
                    self.ident_exists(*ident)?;
                } else {
                    self.visit_expression(e1)?;
                }
                exprs
                    .iter_mut()
                    .try_for_each(|expr| self.visit_expression(expr))
            }
            ExpressionKind::Field(e1, _) => {
                self.visit_expression(e1)
                // the actual field ident should be checked during typechecking, I think?
            }
            ExpressionKind::Identifier(ident) => self.ident_exists(*ident),
            ExpressionKind::Record(record) => record
                .iter_mut()
                .map(|(_, e)| e)
                .try_for_each(|expr| self.visit_expression(expr)),
            ExpressionKind::Unary(_, e1) => self.visit_expression(e1),
            _ => Ok(()),
        }
    }

    fn visit_identifier(&mut self, ident: &mut Identifier) -> VResult {
        self.ident_exists(*ident)
    }

    fn visit_integer(&mut self, _: &mut Integer) -> VResult {
        Ok(())
    }
}
/*
pub trait Visitable {
    fn visit(&mut self, v: &mut dyn Visitor) -> VResult;
}

impl Visitable for Identifier {
    fn visit(&mut self, v: &mut dyn Visitor) -> VResult {
        v.visit_identifier(self)
    }
}

impl Visitable for Integer {
    fn visit(&mut self, v: &mut dyn Visitor) -> VResult {
        v.visit_integer(self)
    }
}

impl<T: Visitable> Visitable for Box<T> {
    fn visit(&mut self, v: &mut dyn Visitor) -> VResult {
        self.visit(v)
    }
}

impl Visitable for Expression {
    fn visit(&mut self, v: &mut dyn Visitor) -> VResult {
        match self {
            Expression::Integer(i) => i.visit(v),
            Expression::Identifier(i) => i.visit(v),
            Expression::Binary(e1, op, e2) => {
                e1.visit(v)?;

            }
        }
    }
}*/
