use crate::{
    ast,
    token::{Range, Token, TokenKind},
};
use logos::{Lexer, Logos};
use std::{borrow::Cow, convert::TryFrom};

#[derive(Clone, Debug)]
pub struct ParseError<'src> {
    pub msg: Cow<'static, str>,
    pub token: Token<'src>,
}

impl<'src> From<Token<'src>> for ParseError<'src> {
    fn from(token: Token<'src>) -> Self {
        Self {
            msg: format!("Unexpected token `{:?}`", token.kind).into(),
            token,
        }
    }
}

impl<'src> ParseError<'src> {
    pub fn new(msg: impl Into<Cow<'static, str>>, token: Token<'src>) -> Self {
        Self {
            msg: msg.into(),
            token,
        }
    }

    pub fn span(&self) -> codespan::ByteSpan {
        self.token.span
    }

    pub fn token(&self) -> Token<'src> {
        self.token
    }
}

impl std::fmt::Display for ParseError<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", &*self.msg)
    }
}

type PResult<'a, T> = Result<T, ParseError<'a>>;

pub struct Parser<'src> {
    lexer: Lexer<TokenKind, &'src str>,
    peek: Option<Token<'src>>,
}

impl<'s> Parser<'s> {
    pub fn new(src: &'s str) -> Self {
        let lexer = TokenKind::lexer(src);
        let peek = Some(Token::from(&lexer));
        Self { lexer, peek }
    }

    pub fn parse_program(&mut self) -> PResult<'s, ast::Program> {
        let mut peek = self.peek();
        let mut functions = Vec::new();
        loop {
            if let Err(ParseError {
                token:
                    Token {
                        kind: TokenKind::Eof,
                        ..
                    },
                ..
            }) = &peek
            {
                break;
            }

            functions.push(self.parse_function()?);

            peek = self.peek();
        }

        Ok(ast::Program(functions))
    }

    fn parse_function(&mut self) -> PResult<'s, ast::Function> {
        let name = self.parse_identifier()?;
        self.eat(TokenKind::LParen)?;
        let parameters = self.parse_ident_list()?;
        self.eat(TokenKind::RParen)?;
        self.eat(TokenKind::LBrace)?;

        let locals = if let TokenKind::Var = self.peek()?.kind {
            self.eat(TokenKind::Var)?;
            let list = self.parse_ident_list()?;
            self.eat(TokenKind::Semicolon)?;

            list
        } else {
            Vec::new()
        };

        let body = self.parse_statement_list()?;

        self.eat(TokenKind::Return)?;
        let ret = self.parse_expression()?;
        self.eat(TokenKind::Semicolon)?;
        let end = self.eat(TokenKind::RBrace)?;

        let span = name.span.to(end.span);

        Ok(ast::Function {
            name,
            parameters,
            locals,
            body,
            ret,
            span,
        })
    }

    fn parse_statement(&mut self) -> PResult<'s, ast::Statement> {
        let peek = self.peek()?;

        Ok(match peek.kind {
            TokenKind::Identifier => {
                let ident = self.parse_identifier()?;
                self.eat(TokenKind::Assign)?;
                let expr = self.parse_expression()?;
                let end = self.eat(TokenKind::Semicolon)?.span;

                let span = ident.span.to(end);

                ast::Statement {
                    kind: ast::StatementKind::Assignment(ident, expr),
                    span,
                }
            }
            TokenKind::Output => {
                let start = self.eat(TokenKind::Output)?.span;
                let expr = self.parse_expression()?;
                let span = start.to(expr.span);
                ast::Statement {
                    kind: ast::StatementKind::Output(expr),
                    span,
                }
            }
            TokenKind::If => {
                let start = self.eat(TokenKind::If)?.span;
                self.eat(TokenKind::LParen)?;
                let expr = self.parse_expression()?;
                self.eat(TokenKind::RParen)?;
                self.eat(TokenKind::LBrace)?;
                let stmt_list = self.parse_statement_list()?;
                let maybe_end = self.eat(TokenKind::RBrace)?.span;

                let (else_block, else_end) = if let TokenKind::Else = self.peek()?.kind {
                    self.eat(TokenKind::Else)?;
                    self.eat(TokenKind::LBrace)?;
                    let list = Some(self.parse_statement_list()?);
                    let end = Some(self.eat(TokenKind::RBrace)?.span);

                    (list, end)
                } else {
                    (None, None)
                };

                let span = if let Some(else_span) = else_end {
                    start.to(else_span)
                } else {
                    start.to(maybe_end)
                };

                ast::Statement {
                    kind: ast::StatementKind::If(expr, stmt_list, else_block),
                    span,
                }
            }
            TokenKind::While => {
                let start = self.eat(TokenKind::While)?.span;
                self.eat(TokenKind::LParen)?;
                let expr = self.parse_expression()?;
                self.eat(TokenKind::RParen)?;
                self.eat(TokenKind::LBrace)?;
                let stmt_list = self.parse_statement_list()?;
                let end = self.eat(TokenKind::RBrace)?.span;

                let span = start.to(end);

                ast::Statement {
                    kind: ast::StatementKind::While(expr, stmt_list),
                    span,
                }
            }
            TokenKind::Star => {
                let start = self.eat(TokenKind::Star)?.span;
                let ident = self.parse_identifier()?;
                self.eat(TokenKind::Assign)?;
                let expr = self.parse_expression()?;
                let end = self.eat(TokenKind::Semicolon)?.span;
                let span = start.to(end);

                ast::Statement {
                    kind: ast::StatementKind::AssignThrough(ident, expr),
                    span
                }
            }
            _ => return Err(peek.into()),
        })
    }

    fn parse_statement_list(&mut self) -> PResult<'s, ast::Statements> {
        let mut statements = Vec::new();

        while self.peek()?.kind != TokenKind::RBrace && self.peek()?.kind != TokenKind::Return {
            statements.push(self.parse_statement()?);
        }

        Ok(statements)
    }

    fn parse_expression(&mut self) -> PResult<'s, ast::Expression> {
        let start = self.parse_primary()?;
        self.parse_expression_inner(start, 0)
    }

    fn parse_expression_inner(
        &mut self,
        mut lhs: ast::Expression,
        min_prec: u8,
    ) -> PResult<'s, ast::Expression> {
        let mut peek = self.peek()?;

        let continue_loop = |token| match ast::BinaryOp::try_from(token) {
            Ok(op) if op.precedence() >= min_prec => true,
            _ => false,
        };

        while continue_loop(peek) {
            let next = self.next()?;
            let op = ast::BinaryOp::try_from(next).unwrap();

            if let ast::BinaryOp::Paren = op {
                let (expr_list, end) = self.parse_expr_list()?;
                let span = lhs.span.to(end);
                lhs = ast::Expression {
                    kind: ast::ExpressionKind::Call(Box::new(lhs), expr_list),
                    span,
                };
                peek = self.peek()?;
            } else {
                let mut rhs = self.parse_primary()?;

                peek = self.peek()?;

                while let (true, prec) = match ast::BinaryOp::try_from(peek) {
                    Ok(op2) if op2.precedence() > op.precedence() => (true, op2.precedence()),
                    _ => (false, 0),
                } {
                    rhs = self.parse_expression_inner(rhs, prec)?;
                    peek = self.peek()?;
                }

                if let ast::BinaryOp::Dot = op {
                    if let ast::ExpressionKind::Identifier(ident) = rhs.kind {
                        let span = lhs.span.to(ident.span);
                        lhs = ast::Expression {
                            kind: ast::ExpressionKind::Field(Box::new(lhs), ident),
                            span,
                        };
                    } else {
                        return Err(ParseError::new(
                            format!("Expected identifier, found expression: {:?}", rhs),
                            next,
                        ));
                    }
                } else {
                    let span = lhs.span.to(rhs.span);
                    lhs = ast::Expression {
                        kind: ast::ExpressionKind::Binary(Box::new(lhs), op, Box::new(rhs)),
                        span
                    };
                }
            }
        }

        Ok(lhs)
    }

    fn parse_primary(&mut self) -> PResult<'s, ast::Expression> {
        let peek = self.peek()?;

        Ok(match peek.kind {
            TokenKind::Identifier => {
                let ident = self.parse_identifier()?;
                let span = ident.span;
                ast::Expression {
                    kind: ast::ExpressionKind::Identifier(ident),
                    span,
                }
            }
            TokenKind::Input => {
                let span = self.eat(TokenKind::Input)?.span;
                ast::Expression {
                    kind: ast::ExpressionKind::Input,
                    span,
                }
            }
            TokenKind::Integer => {
                let int = self.parse_integer()?;
                let span = int.span;
                ast::Expression {
                    kind: ast::ExpressionKind::Integer(int),
                    span,
                }
            }
            TokenKind::Null => {
                let span = self.eat(TokenKind::Null)?.span;
                ast::Expression {
                    kind: ast::ExpressionKind::Null,
                    span,
                }
            }
            TokenKind::LBrace => self.parse_record()?,
            TokenKind::Alloc => {
                let start = self.eat(TokenKind::Alloc)?.span;
                let expr = self.parse_expression()?;
                let span = start.to(expr.span);
                ast::Expression {
                    kind: ast::ExpressionKind::Unary(ast::UnaryOp::Alloc, Box::new(expr)),
                    span,
                }
            }
            TokenKind::LParen => {
                let start = self.eat(TokenKind::LParen)?.span;
                let mut expr = self.parse_expression()?;
                let end = self.eat(TokenKind::RParen)?.span;

                expr.span = start.to(end);

                expr
            }
            TokenKind::Ref => {
                let start = self.eat(TokenKind::Ref)?.span;
                let ident = self.parse_identifier()?;
                let span = start.to(ident.span);

                ast::Expression {
                    kind: ast::ExpressionKind::Unary(
                    ast::UnaryOp::Ref,
                    Box::new(ast::Expression {
                        kind: ast::ExpressionKind::Identifier(ident),
                        span: ident.span,
                    }),
                ),
                span
                }
            }
            TokenKind::Star => {
                let start = self.eat(TokenKind::Star)?.span;
                let prim = self.parse_primary()?;
                // Same precedence as `(` so it has lower precedence than
                // `.` or a function call
                let inner = self.parse_expression_inner(prim, ast::BinaryOp::Paren.precedence())?;
                let span = start.to(inner.span);
                ast::Expression {
                    kind: ast::ExpressionKind::Unary(
                    ast::UnaryOp::Deref,
                    Box::new(inner),
                ),
                span,
                }
            }
            TokenKind::Minus => {
                let start = self.eat(TokenKind::Minus)?.span;
                let peek = self.peek()?;

                if let TokenKind::Integer = peek.kind {
                    let int = -self.parse_integer()?;
                    let span = start.to(int.span);
                    ast::Expression {
                        kind: ast::ExpressionKind::Integer(int),
                        span,
                    }
                } else {
                    let inner = self.parse_expression()?;
                    let span = start.to(inner.span);
                    ast::Expression {
                        kind: ast::ExpressionKind::Unary(ast::UnaryOp::Negate, Box::new(inner)),
                        span,
                    }
                }
            }
            _ => return Err(peek.into()),
        })
    }

    fn parse_record(&mut self) -> PResult<'s, ast::Expression> {
        let start = self.eat(TokenKind::LBrace)?.span;

        let mut record_elements = Vec::new();

        while self.peek()?.kind != TokenKind::RBrace {
            let ident = self.parse_identifier()?;
            self.eat(TokenKind::Colon)?;
            let expr = self.parse_expression()?;

            record_elements.push((ident, expr));

            if let TokenKind::Comma = self.peek()?.kind {
                self.eat(TokenKind::Comma)?;
            } else {
                break;
            }
        }

        let end = self.eat(TokenKind::RBrace)?.span;

        let span = start.to(end);

        Ok(ast::Expression {
                        kind: ast::ExpressionKind::Record(record_elements),
                        span,
        })
    }

    fn parse_expr_list(&mut self) -> PResult<'s, (Vec<ast::Expression>, codespan::ByteSpan)> {
        //self.eat(TokenKind::LParen)?;

        let mut exprs = Vec::new();

        while self.peek()?.kind != TokenKind::RParen {
            let expr = self.parse_expression()?;

            exprs.push(expr);

            if let TokenKind::Comma = self.peek()?.kind {
                self.eat(TokenKind::Comma)?;
            } else {
                break;
            }
        }

        let end = self.eat(TokenKind::RParen)?.span;

        Ok((exprs, end))
    }

    fn parse_ident_list(&mut self) -> PResult<'s, Vec<ast::Identifier>> {
        let mut idents = Vec::new();

        while self.peek()?.kind != TokenKind::RParen {
            let ident = self.parse_identifier()?;

            idents.push(ident);

            if let TokenKind::Comma = self.peek()?.kind {
                self.eat(TokenKind::Comma)?;
            } else {
                break;
            }
        }

        Ok(idents)
    }

    fn parse_integer(&mut self) -> PResult<'s, ast::Integer> {
        let tkn = self.next()?;

        if let TokenKind::Integer = tkn.kind {
            Ok(ast::Integer{
                value: tkn.slice.parse().unwrap(),
                span: tkn.span
                })
        } else {
            Err(tkn.into())
        }
    }

    fn parse_identifier(&mut self) -> PResult<'s, ast::Identifier> {
        let tkn = self.next()?;

        if let TokenKind::Identifier = tkn.kind {
            Ok(ast::Identifier::new(tkn.slice, tkn.span))
        } else {
            Err(tkn.into())
        }
    }

    fn eat(&mut self, tk: TokenKind) -> PResult<'s, Token<'s>> {
        let tkn = self.next()?;

        if tkn.kind == tk {
            Ok(tkn)
        } else {
            Err(tkn.into())
        }
    }

    fn peek(&mut self) -> PResult<'s, Token<'s>> {
        if let Some(tkn) = self.peek {
            Ok(tkn)
        } else {
            let tkn = self.next()?;
            self.peek = Some(tkn);

            Ok(tkn)
        }
    }

    fn next(&mut self) -> PResult<'s, Token<'s>> {
        if self.peek.is_some() {
            Ok(self.peek.take().unwrap())
        } else {
            self.lexer.advance();

            let token = Token::from(&self.lexer);

            match self.lexer.token {
                TokenKind::Error => Err(ParseError::new(
                    format!("Unknown start of token `{}`", self.lexer.slice()),
                    token,
                )),
                TokenKind::Eof => Err(ParseError::new("Unexpected end of file", token)),
                _ => Ok(token),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn simple_expr() {
        let mut parser = Parser::new(r#"1 + -2 * ident / -4 > 5 == (-6 + otherident) / 7 alloc"#);

        let res = parser.parse_expression();

        let res = res.unwrap();
        panic!("{:#?}", res);
    }

    #[test]
    fn fn_call() {
        let mut parser = Parser::new("a(b) + (1 + 2);");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));

        let mut parser = Parser::new("a(b) + q(r);");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));

        let mut parser = Parser::new("a(b(c(d)));");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));

        let mut parser = Parser::new("(1+2)(a, b, c, d);");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));

        let mut parser = Parser::new("(1+2)(a, b)();");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));

        let mut parser = Parser::new("*a(b);");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));
        panic!("{:#?}", res);
    }

    #[test]
    fn field_access() {
        let mut parser = Parser::new("a.b;");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));

        let mut parser = Parser::new("a.b.a;");
        let res = parser.parse_expression();
        assert!(res.is_ok(), format!("{:#?}", res));
    }
}
