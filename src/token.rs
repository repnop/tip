use logos::{Lexer, Logos};

#[derive(Clone, Copy, Debug, Logos, PartialEq)]
pub enum TokenKind {
    #[error]
    Error,
    #[end]
    Eof,

    #[regex = "[0-9]+"]
    Integer,
    #[regex = "[a-z]+"]
    Identifier,
    #[token = "("]
    LParen,
    #[token = ")"]
    RParen,
    #[token = "{"]
    LBrace,
    #[token = "}"]
    RBrace,
    #[token = "+"]
    Plus,
    #[token = "-"]
    Minus,
    #[token = "*"]
    Star,
    #[token = "/"]
    Div,
    #[token = ">"]
    GreaterThan,
    #[token = "=="]
    Equal,
    #[token = "="]
    Assign,
    #[token = ":"]
    Colon,
    #[token = ";"]
    Semicolon,
    #[token = "."]
    Dot,
    #[token = ","]
    Comma,
    #[token = "&"]
    Ref,

    #[token = "input"]
    Input,
    #[token = "output"]
    Output,
    #[token = "if"]
    If,
    #[token = "else"]
    Else,
    #[token = "while"]
    While,
    #[token = "var"]
    Var,
    #[token = "return"]
    Return,
    #[token = "alloc"]
    Alloc,
    #[token = "null"]
    Null,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Range {
    pub start: usize,
    pub end: usize,
}

impl From<std::ops::Range<usize>> for Range {
    fn from(r: std::ops::Range<usize>) -> Self {
        Self {
            start: r.start,
            end: r.end,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Token<'src> {
    pub kind: TokenKind,
    pub slice: &'src str,
    pub span: codespan::ByteSpan,
}

impl<'src> From<&'_ Lexer<TokenKind, &'src str>> for Token<'src> {
    fn from(lexer: &Lexer<TokenKind, &'src str>) -> Self {
        Self {
            kind: lexer.token,
            slice: lexer.slice(),
            span: codespan::ByteSpan::new(
                codespan::ByteIndex(if lexer.range().end - lexer.range().start >= 1 {
                    lexer.range().start as u32 + 1
                } else {
                    lexer.range().start as u32
                }),
                codespan::ByteIndex(lexer.range().end as u32 + 1),
            ),
        }
    }
}
