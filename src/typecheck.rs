#[derive(Debug)]
struct UnionFind<T> {
    storage: Vec<Set<T>>,
}

impl<T> UnionFind<T> {
    fn new() -> Self {
        Self {
            storage: Vec::new(),
        }
    }

    fn make_set(&mut self, val: T) -> Key<T> {
        let parent = self.storage.len();
        let set = Set {
            val,
            parent,
            rank: 0,
        };

        self.storage.push(set);

        Key {
            _marker: std::marker::PhantomData,
            id: parent,
        }
    }

    fn find(&mut self, key: Key<T>) -> Key<T> {
        if self.storage[key.id].parent != key.id {
            self.storage[key.id].parent = self.find(Key {
                _marker: std::marker::PhantomData,
                id: self.storage[key.id].parent,
            }).id;
        }
        
        let parent = self.storage[key.id].parent;
        
        Key {
            _marker: std::marker::PhantomData,
            id: parent,
        }
    }
    
    fn union(&mut self, x: Key<T>, y: Key<T>) {
        let mut x_id = self.find(x).id;
        let mut x_root = self.storage[x_id];
        
        let mut y_id = self.find(y).id;
        let mut y_root = self.storage[y_id];
        
        if x_root != y_root {
            if x_root.rank != y_root.rank {
                self.storage.swap(x_id, y_id);
            }
            
            std::mem::swap(&mut x_root, &mut y_root);
            std::mem::swap(&mut x_id, &mut y_id);
            
            self.storage[y_id].parent = x_id;
            
            if x_root.rank == y_root.rank {
                self.storage[x_id].rank += 1;
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Key<T> {
    _marker: std::marker::PhantomData<T>,
    id: usize,
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Set<T> {
    val: T,
    parent: usize,
    rank: usize,
}

pub enum Type {
    Int,
    Null,
    Ref(Box<Type>),
    Func(Vec<Box<Type>>, Box<Type>),
}

pub struct TypeVar {
    id: i32,
    resolved: Option<Type>,
}

pub struct TypeChecker {
    type_table: 
}